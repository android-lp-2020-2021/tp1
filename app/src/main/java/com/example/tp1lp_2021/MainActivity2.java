package com.example.tp1lp_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Person;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tp1lp_2021.model.Personne;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        //String message = intent.getStringExtra("message");
        //TextView textView = findViewById(R.id.tvAct2message);
        //textView.setText(message);
        Personne personne = intent.getParcelableExtra("personne");

        Log.d("MesLogs",personne.getNom()+ " "+ personne.getPrenom());

    }

    public void retour(View view) {
        finish();
    }

    public void repondre(View view) {
        EditText editText = findViewById(R.id.editTextReponse);
        String reponse = editText.getText().toString();
        Intent intent =new Intent();
        intent.putExtra("reponse",reponse);
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }
}