package com.example.tp1lp_2021.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Personne implements Parcelable {
    private String nom;
    private String  prenom;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Personne(Parcel in){
        this.nom = in.readString();
        this.prenom = in.readString();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(prenom);
    }

    public static final Parcelable.Creator<Personne> CREATOR
            = new Parcelable.Creator<Personne>() {
        public Personne createFromParcel(Parcel in) {
            return new Personne(in);
        }

        public Personne[] newArray(int size) {
            return new Personne[size];
        }
    };
}
