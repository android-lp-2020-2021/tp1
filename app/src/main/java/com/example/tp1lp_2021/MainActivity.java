package com.example.tp1lp_2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tp1lp_2021.model.Personne;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private TextView nbrotations;
    private Button bPhoto;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nbrotations = findViewById(R.id.tvnbrotations);
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                echange(v);
            }
        });
        Toast.makeText(this,"Passage onCreate.",Toast.LENGTH_SHORT).show();
        Log.d("MesLogs","Passage onCreate.");
        bPhoto = findViewById(R.id.buttonPhoto);
        bPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, 1);
                }else{
                    Toast.makeText(getApplicationContext(),"impossible de prendre une photo",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onStart(){
        super.onStart();
        Toast.makeText(this,"Passage onStart.",Toast.LENGTH_SHORT).show();
        Log.d("MesLogs","Passage onStart.");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("MesLogs","Passage onResume.");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d("MesLogs","Passage onStop.");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d("MesLogs","Passage onDestroy.");
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.d("MesLogs","Passage onRestart.");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d("MesLogs","Passage onPause.");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d("MesLogs","Passage onSaveInstanceState");
        outState.putString("nbrotations", nbrotations.getText().toString());


        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d("MesLogs","Passage onRestaureInstanceState");

        String nb = savedInstanceState.getString("nbrotations");
        Integer nbr = Integer.valueOf(nb);
        nbr++;
        nbrotations.setText(nbr.toString());
    }

    public void echange(View view) {
        Log.d("MesLogs","Debut echange");
        TextView tv1 = findViewById(R.id.tv1);
        TextView tv2 = findViewById(R.id.tv2);
        String sauv = tv1.getText().toString();
        tv1.setText(tv2.getText().toString());
        tv2.setText(sauv);
        Toast.makeText(this,"Echange des messages.",Toast.LENGTH_SHORT).show();
        Log.d("MesLogs","Fin echange");
    }

    public void dialPhoneNumber(String phoneNumber) {
        Log.d("MesLogs","debut dial " + phoneNumber);
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            Log.d("MesLogs","startactivity telephone");
            startActivity(intent);
        }
    }

    public void appeler(View view) {
        EditText editText = findViewById(R.id.edTNumber);
        Log.d("MesLogs","appel methode dial");
        dialPhoneNumber(editText.getText().toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("MesLogs","resultcode = "+resultCode);
        Log.d("MesLogs","resultcode = "+RESULT_OK);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            bitmap = (Bitmap) extras.get("data");
            ImageView imageView = findViewById(R.id.imageView);
            imageView.setImageBitmap(bitmap);
        }else {
         if(requestCode==2 && resultCode == RESULT_OK){
             EditText editText = findViewById(R.id.editTextMessage);
             String rep = data.getStringExtra("reponse");
             editText.setText(rep);
         }
         else{
                Toast.makeText(getApplicationContext(), "pb avec la reponse attendue", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void goToActivityTwo(View view) {
        startActivity(new Intent(this,MainActivity2.class));
    }

    public void envoyerMessage(View view) {
        EditText editText = findViewById(R.id.editTextMessage);
        String message = editText.getText().toString();
        Intent intent = new Intent(this, MainActivity2.class);
        intent.putExtra("message",message);
        startActivityForResult(intent,2);
    }

    public void envoyerPersonne(View view) {
        EditText etnom = findViewById(R.id.editTextNom);
        String nom = etnom.getText().toString();
        EditText etprenom = findViewById(R.id.editTextTextPrenom);
        String prenom = etprenom.getText().toString();
        Personne personne = new Personne(nom,prenom);
        Intent intent = new Intent(this,MainActivity2.class);
        intent.putExtra("personne",personne);
        startActivity(intent);
    }
}